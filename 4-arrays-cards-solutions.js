const cardsData = require("./4-arrays-cards");

// 1. Find all card numbers whose sum of all the even position digits is odd.

const cardNums = cardsData.filter((card)=>{
    let cardNum = [...card.card_number];
    let sum = cardNum.reduce((acc, value, i)=>{
        if(i%2 != 0){
            value = 0;
        }
        return acc + parseInt(value);
    },0);

    return sum % 2 != 0;

}).map(data=>{
    return data.card_number;
});

console.log("1. Card numbers whose sum of all the even position digits is odd.\n");
console.log(cardNums);
console.log("\n\n");

//2. Find all cards that were issued before June.

const cardsIssuesBeforeJune = cardsData.filter((card)=>{
    let month = parseInt(card.issue_date.split("/")[0]);
    return month < 6 ;
});

console.log("2. Cards that were issued before June:\n");
console.log(cardsIssuesBeforeJune);
console.log("\n\n");

// 3. Assign a new field to each card for CVV where the CVV is a random 3 digit number.

const cardsWithCVV = cardsData.map((data)=>{
    let cvv = (Math.random()*1000).toFixed(0);
    data["CVV"] = cvv;
    return data;

});

console.log("3. Cards data with CVV:\n");
console.log(cardsWithCVV);
console.log("\n\n");



//4. Add a new field to each card to indicate if the card is valid or not.

const modifiedCardDataWithValidity = cardsData.map((data)=>{
    data['is_valid'] = "";
    return data;
})

console.log("4. Modified cards with validity field:\n");
console.log(modifiedCardDataWithValidity);
console.log("\n\n");



//5. Invalidate all cards issued before March.


const invaidCards = cardsData.map((data)=>{
    let month = parseInt(data.issue_date.split("/")[0]);
    if(month < 3){
        data['is_valid'] = false;
    }
    else{
        data['is_valid'] = true;
    }
    return data;
})

console.log("5. Invalidated cards issued before March.\n");
console.log(invaidCards);
console.log("\n\n");


//6. Sort the data into ascending order of issue date.

const sortedCardsData = cardsData.map((data)=>{
let date = data.issue_date.split("/") ;
if(parseInt(date[0])<10){
    date[0] = "0" + date[0];
}
if(parseInt(date[1])<10){
    date[1] = "0" + date[1];
}
data["issue_date2"] = `${date[2]}/${date[0]}/${date[1]}`;
return data;
}).sort((data1, data2)=>{
    let date1 = data1.issue_date2;
    let date2 = data2.issue_date2;
    let ans = date1 < date2 ? -1 : date1 == date2 ?  0 : 1;
    return ans;
}).map(data=>{
    delete data.issue_date2;
    return data;
});

console.log("6. Sorted data by issue date.\n");
console.log(sortedCardsData);
console.log("\n\n");

//7. Group the data in such a way that we can identify which cards were assigned in which months.

const groupedData = cardsData.reduce((acc, data)=>{
    let month = data.issue_date.split("/")[0];
    return {...acc, [month]:[...acc[month]||[], data ]}
},{});

console.log("7.  Grouped data by issuing month:\n");
console.log(groupedData);
console.log("\n\n");
